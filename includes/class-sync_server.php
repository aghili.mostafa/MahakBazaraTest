<?php

/**
 * Created by PhpStorm.
 * User: aghili
 * Date: 7/14/2018
 * Time: 3:15 PM
 */
class sync_server
{
    private $options;


    public function __construct(&$option)
    {
        $this->options = $option;
    }

    function CallAPI($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    public function login($user_name,$password)
    {
        $send_arr = [
            'username' => $user_name,
            'password' => $password
        ];
        $data = http_build_query($send_arr);
        $response = $this->CallAPI('POST', $this->options->server_uri+'/sync/Login', $data);
        if ($response) {
            $response = json_decode($response);
            return $response;
        };
        return false;
    }

    public function get_products(){
        $send_arr = [
            'systemSyncID' => $this->options->systemSyncID,
            'changeAfter' => $this->options->last_sync_time,
            'userToken' => $this->options->token
        ];
        $data = http_build_query($send_arr);
        $response = $this->CallAPI('POST', $this->server_uri+'/sync/GetProducts', $data);
        if ($response) {
            $response = json_decode($response);
            if ($response->result === 'success') {
                $this->token = $response->data->token;
                return true;
            };
        };
        return false;
    }
}