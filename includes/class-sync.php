<?php

/**
 * Created by PhpStorm.
 * User: aghili
 * Date: 7/14/2018
 * Time: 2:52 PM
 */

class sync
{

    private $options;

    public function __construct(&$options)
    {

        $this->options = $options;
        if ($options->sync_list == null)
            $options->sync_list = [];
        else
            $this->sync_list = $options->sync_list;
    }

    public function need_sync()
    {
        return time() - $this->options->last_sync_time > $this->options->sync_period_time;
    }

    public function has_record_for_update()
    {
        return count($this->options->sync_list) > 0;
    }

    public function get_sync()
    {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sync-server.php';
        $sync_server = new sync_server($this->options);
        if(!$this->options->token) {
            if (!$response = $sync_server->login($this->options->user_name, $this->options->password)) return false;
            if (!$response->token) return false;
            // set token to option array for used in get_products function
            $this->options->token = $response->token;
        }
        $response = $sync_server->get_products();
        if($response->result==='success'){
            foreach($response->data as $product) {
                $this->options->sync_list[$product->ProductID] = $product;
            }
            return true;
        }
        return false;
    }


    public function update_records()
    {
        foreach($this->options as $product) {
            $this->options->sync_list[$product->ProductID];
        }
    }
}